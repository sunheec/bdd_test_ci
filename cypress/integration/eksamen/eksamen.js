import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";

Given(/^at jeg har åpent nettsiden$/, () => {
  cy.visit("http://localhost:8080");
});
When(/^jeg fyller ut navn, kjønn, beløp$/, () => {
  cy.get(`input[name="name"]`).type("sunhee");
  cy.get(`select[name="sex"]`).select("Kvinne");
  cy.get(`input[name="amount"]`).type("1000");
});
And(/^jeg trykker på send inn$/, () => {
  cy.get(".formButton").click();
});
Then(/^skal jeg beskjed om at søknaden er sendt$/, () => {
  cy.get("#resultat").should("exist");
});

Given(/^at jeg har åpent nettsiden$/, () => {
  cy.visit("http://localhost:8080");
});
When(/^jeg ikke legger inn verdier i feltene$/, () => {
  cy.get(`select[name="sex"]`).select("Mann");
});
And(/^jeg tykker på Send inn$/, () => {
  cy.get(".formButton").click();
});
Then(/^skal jeg ikke få meldinger om godkjentsøknad $/, () => {
  cy.contains("#resultat", "Du fikk innvilget søknaden").should("not.exist");
});
