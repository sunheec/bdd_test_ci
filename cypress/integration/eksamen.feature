# language: no
Egenskap: Søknad

  Som bruker
  Ønsker jeg å sende inn lånkesøknad
  Slik at jeg kan søke på lån


  Scenario: Fylle ut skjema
    Gitt at jeg har åpent nettsiden
    Når jeg fyller ut navn, kjønn, beløp
    Og jeg trykker på send inn
    Så skal jeg beskjed om at søknaden er sendt 
  
  Scenario: 
    Gitt at jeg har åpent nettsiden
    Når jeg ikke legger inn verdier i feltene
    Og jeg tykker på Send inn
    Så skal jeg ikke få meldinger om godkjentsøknad 
