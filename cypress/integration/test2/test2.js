import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";

Given(/^at jeg har åpnet nettkiosken$/, () => {
  cy.visit("http://localhost:8080");
});

And(/^jeg har lagt inn varer i handlekurven$/, () => {
  cy.get("#product").select("Hobby");
  cy.get("#quantity").clear().type("2");
  cy.get("#saveItem").click();
  cy.get("#product").select("Smørbukk");
  cy.get("#quantity").clear().type("8");
  cy.get("#saveItem").click();
});

And(/^trykket på Gå til betaling$/, () => {
  cy.get("#goToPayment").click();
});

When(
  /^jeg legger inn navn, adresse, postnummer, poststed og kortnummer$/,
  () => {
    cy.get("#fullName").type("sunhee");
    cy.get("#address").type("gatagata11");
    cy.get("#postCode").type("4567");
    cy.get("#city").type("oslo");
    cy.get("#creditCardNo").type("1234567890123456");
  }
);

And(/^jeg trykker på Fullfør kjøp$/, () => {
  cy.get("input[type=submit]").click();
});

Then(/^skal jeg få beskjed om at kjøpet er registrert$/, () => {
  cy.get(".confirmation").contains("Din ordre er registrert").should("exist");
});

Given(/^at jeg har åpnet nettkiosken$/, () => {
  cy.visit("http://localhost:8080");
});

And(/^jeg har lagt inn varer i handlekurven$/, () => {
  cy.get("#product").select("Smil");
  cy.get("#quantity").clear().type("1");
  cy.get("#saveItem").click();

  cy.get("#product").select("Hobby");
  cy.get("#quantity").clear().type("2");
  cy.get("#saveItem").click();
});

And(/^jeg har trykket på Gå til betaling$/, () => {
  cy.get("#goToPayment").click();
});

When(/^jeg legger inn ugyldige verdier i feltene$/, () => {
  cy.get("#fullName").clear().type("SUNHEE CHO");
  cy.get("#address").clear().type("ABCD gata");
  cy.get("#postCode").clear().type("1234");
  cy.get("#city").clear().type("OSLO");
  cy.get("#creditCardNo").clear().type("123456");
});

And(/^jeg trykker på Fullfør kjøp$/, () => {
  cy.get("input[type=submit]").click();
});

Then(/^skal jeg få feilmeldinger for disse$/, () => {
  cy.get("#creditCardNoError").should("not.be.empty");
});

// Given(/^at jeg har åpnet nettkiosken$/, () => {
//   cy.visit("http://localhost:8080");
// });
// And(/^jeg har lagt inn varer i handlekurven$/, () => {});
// And(/^jeg har trykket på Gå til betaling$/, () => {});
// When(/^jeg legger inn ugyldige verdier i feltene$/, () => {});
// And(/^jeg trykker på Fullfør kjøp$/, () => {});
// Then(/^skal jeg få feilmeldinger for disse$/, () => {});
