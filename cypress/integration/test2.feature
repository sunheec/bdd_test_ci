# language: no
Egenskap: beskjed om kjøp

  Som kunde av nettkiosken
  Ønsker jeg å kunne legge inn betalingsinformasjon
  Slik at jeg kan få godis i posten

  Scenario: 
    Gitt at jeg har åpnet nettkiosken
    Og jeg har lagt inn varer i handlekurven
    Og trykket på Gå til betaling
    Når jeg legger inn navn, adresse, postnummer, poststed og kortnummer
    Og jeg trykker på Fullfør kjøp
    Så skal jeg få beskjed om at kjøpet er registrert

  Scenario: 
    Gitt at jeg har åpnet nettkiosken
    Og jeg har lagt inn varer i handlekurven
    Og jeg har trykket på Gå til betaling
    Når  jeg legger inn ugyldige verdier i feltene
    Og jeg trykker på Fullfør kjøp
    Så skal jeg få feilmeldinger for disse