import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";

Given(/^at jeg har åpnet nettkiosken$/, () => {
  cy.visit("http://localhost:8080");
});

When(/^jeg legger inn varer og kvanta$/, () => {
  cy.get("#product").select("Stratos");
  cy.get("#quantity").clear().type("2");
  cy.get("#saveItem").click();
  cy.get("#product").select("Smørbukk");
  cy.get("#quantity").clear().type("4");
  cy.get("#saveItem").click();
});

Then(/^skal handlekurven inneholde det jeg har lagt inn$/, () => {
  cy.get("#list").should("contain", "2 Stratos; 16 kr");
  cy.get("#list").should("contain", "4 Smørbukk; 4 kr");
});

And(/^den skal ha riktig totalpris$/, () => {
  cy.get("#price").should("have.text", "20");
});

Given(/^at jeg har åpnet nettkiosken$/, () => {
  cy.visit("http://localhost:8080");
});

And(/^jeg legger inn varer og kvanta$/, () => {
  cy.get("#product").select("Smil");
  cy.get("#quantity").clear().type("1");
  cy.get("#saveItem").click();

  cy.get("#product").select("Hobby");
  cy.get("#quantity").clear().type("2");
  cy.get("#saveItem").click();
});

When(/^jeg sletter varer$/, () => {
  cy.get("#product").select("Smil");
  cy.get("#quantity").clear().type("1");
  cy.get("#deleteItem").click();
});

Then(/^skal ikke handlekurven inneholde det jeg har slettet$/, () => {
  cy.get("#list").should("not.contain", "Smil");
});

Given(/^at jeg har åpnet nettkiosken$/, () => {
  cy.visit("http://localhost:8080");
});

And(/^jeg legger inn varer og kvanta$/, () => {
  cy.get("#product").select("Smil");
  cy.get("#quantity").clear().type("1");
  cy.get("#saveItem").click();

  cy.get("#product").select("Hobby");
  cy.get("#quantity").clear().type("2");
  cy.get("#saveItem").click();
});

When(/^jeg oppdaterer kvanta for en vare$/, () => {
  cy.get("#product").select("Smil");
  cy.get("#quantity").clear().type("2");
  cy.get("#saveItem").click();
});

Then(/^skal handlekurven inneholde riktig kvanta for varen$/, () => {
  cy.get("#list").should("contain", "2 Smil");
});
